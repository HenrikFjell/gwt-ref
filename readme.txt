Project generated with https://github.com/tbroyer/gwt-maven-archetypes

Start the development mode
--------------------------

Change directory to your generated project and issue the following commands:

1. In one terminal window: mvn gwt:codeserver -pl *-client -am
2. In another terminal window: mvn jetty:run -pl *-server -am -Denv=dev
3. In a browser open 127.0.0.1:8080